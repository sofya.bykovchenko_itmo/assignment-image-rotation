#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

//----------------------------------------------------------------//

struct image img_from_file(char *file_path);

bool img_to_file(char *file_path, const struct image img);

// static struct image NULL_IMG = (struct image){
//     .width = 0,
//     .height = 0,
//     .data = NULL
// };

struct image new_img(uint64_t height, uint64_t width);

void free_img(struct image img);


#endif //IMAGE_H
