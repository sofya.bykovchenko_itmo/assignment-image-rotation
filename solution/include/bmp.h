#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H

#include <image.h>
#include <stdint.h>
#include <stdio.h>



enum read_status {
    READ_OK = 0,
    READ_ERROR,
    READ_ERROR_PROBLEM_WITH_FILE_OR_WITH_IMAGE,
    READ_INVALID_HEADER,
    READ_INVALID_BITS,
    READ_INVALID_SIGNATURE
};

enum read_status from_bmp(FILE *in, struct image *img);

enum status_ {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FAILED_IMG_NULL,
    WRITE_FAILED_OUT_NULL,

};

enum status_ bmp_file(FILE *out, const struct image *img);

#endif //IMAGE_ROTATION_BMP_H
