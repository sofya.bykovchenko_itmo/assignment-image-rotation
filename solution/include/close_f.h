#ifndef ASSIGNMENT_IMAGE_ROTATION_CLOSE_F_H
#define ASSIGNMENT_IMAGE_ROTATION_CLOSE_F_H

#include <stdio.h>

enum close_file {
    CLOSE,
    CLOSE_ERROR
};

enum close_file close(FILE **file);

#endif //ASSIGNMENT_IMAGE_ROTATION_CLOSE_F_H
