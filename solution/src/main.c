#include "image.h"
#include "rotate.h"

#include <stdio.h>


int main(int argc, char **argv) {

    //checking the number of arguments
    if (argc != 3) {
        fprintf(stderr, "invalid parametrs");
        return 1;
    }

    //reading picture from file
    struct image img = img_from_file(argv[1]);

    if (img.data == NULL) {
        fprintf(stderr, "can't read from file");
        free_img(img);
        return 1;
    }

    //rotate the image
    struct image rotate_image = rotate(&img);

    // write the picture to file
    if (!img_to_file(argv[2], rotate_image)) {
        fprintf(stderr, "can't write the picture to file");
        free_img(img);
        free_img(rotate_image);
        return 1;
    }

    free_img(img);
    free_img(rotate_image);

    return 0;
}
