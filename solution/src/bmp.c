#include <bmp.h>
#include <stdbool.h>

static const uint16_t bf_Type = 0x4D42;
static const uint16_t bi_Bit_Count = 24;
static const uint16_t bi_Planes = 1;
static const uint32_t bi_Size = 40;
static const uint32_t bf_Reserved = 0;


#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType; // 0x4D42, BM signature
    uint32_t bfileSize;
    uint32_t bfReserved; //0 (reserved word)
    uint32_t bOffBits;  //offset to the data field, bytes
    uint32_t biSize; //structure size, bytes
    uint32_t biWidth; // the width of the bmp image in pixels
    uint32_t biHeight; // the height of the bmp image in pixels
    uint16_t biPlanes; //the number of planes, should =1
    uint16_t biBitCount; //color, number of bits per pixel, =24 (in our case)
    uint32_t biCompression; // compression type in the file, BI_RGB - compression is not used
    uint32_t biSizeImage; // how many bytes are in the data field
    uint32_t biXPelsPerMeter; // X-axis resolution, dots per inch
    uint32_t biYPelsPerMeter; // Y-axis resolution, dots per inch
    uint32_t biClrUsed;
    uint32_t biClrImportant; // the quantity of essential colors, (=0, in this case all colors will be important)
};
#pragma pack(pop)


static struct bmp_header create_header(const struct image *img);

static uint8_t padding_uint8_t(uint64_t width);

static bool check_file_and_image(FILE *in, struct image *img) {
    return in && img;
}

static bool check_bmp(struct bmp_header bmp_header) {
    return bi_Planes == bmp_header.biPlanes && bf_Reserved == bmp_header.bfReserved;
}

static bool check_signature(struct bmp_header bmp_header) {
    return bf_Type == bmp_header.bfType;
}

static bool check_header(struct bmp_header bmp_header) {
    return bi_Size == bmp_header.biSize;
}

static bool check_bits(struct bmp_header bmp_header) {
    return bi_Bit_Count == bmp_header.biBitCount;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmp_header;

    if (!check_file_and_image(in, img)) {
        return READ_ERROR_PROBLEM_WITH_FILE_OR_WITH_IMAGE;
    }

    if (fread(&bmp_header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_ERROR;
    }

    if (!check_bmp(bmp_header)) { return READ_ERROR; }

    if (!check_signature(bmp_header)) { return READ_INVALID_SIGNATURE; }

    if (!check_header(bmp_header)) { return READ_INVALID_HEADER; }

    if (!check_bits(bmp_header)) { return READ_INVALID_BITS; }

    *img = new_img((uint64_t) bmp_header.biHeight, (uint64_t) bmp_header.biWidth);

    if (img->data == NULL) {
        return READ_ERROR;
    }

    uint8_t const padding = padding_uint8_t(img->width);

    for (size_t i = 0; i < img->height; i++) {

        if (fread(&(img->data[i * img->width]), //reading bytes
                  sizeof(struct pixel), img->width, in) != img->width ||
            fseek(in, padding, SEEK_CUR) != 0) {
            free_img(*img);
            return READ_ERROR;
        }
    }

    return READ_OK;
}


static bool check_out(const struct image *img, FILE *out) { return img && out; }


enum status_ bmp_file(FILE *out, const struct image *img) {
    uint8_t const padding = padding_uint8_t(img->width);
    struct bmp_header b_h = create_header(img);

    if (!check_out(img, out)) {
        return WRITE_FAILED_OUT_NULL;
    }

    //writing down the title
    if (fwrite(&b_h, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    //writing down the image
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(
                (uint8_t *) img->data + i * img->width * 3,
                sizeof(struct pixel),
                img->width,
                out) != img->width ||

            fseek(out, padding, SEEK_CUR) != 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}


static uint8_t padding_uint8_t(uint64_t width) {
    return (uint8_t) (width % 4);
}


static struct bmp_header create_header(const struct image *img) {
    uint8_t const padding = padding_uint8_t(img->width);
    struct bmp_header bmp_header = {
            .bfType=bf_Type,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = bi_Size,
            .biWidth = img->width,
            .biHeight = img->height,
            .biBitCount = bi_Bit_Count,
            .bfReserved = 0,
            .biClrImportant = 0,
            .biPlanes = 1,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biCompression = 0,
            .bfileSize = img->height * img->width * sizeof(struct pixel) +
                         img->height * padding
                         + sizeof(struct bmp_header),
            .biSizeImage =  img->height * img->width * sizeof(struct pixel) + padding * img->height
    };

    return bmp_header;

}

