#include "rotate.h"
#include "bmp.h"
#include <stdio.h>


uint64_t pixels_index_of_rot_img(uint32_t i, uint32_t j, struct image image) {
    return (image.width - j - 1) * image.height + i;
}

uint64_t pixels_index_of_img(uint32_t i, uint32_t j, struct image image) {
    return i * image.width + j;
}

struct image rotate(struct image const *rotate_img) {
    struct image image = new_img(rotate_img->width, rotate_img->height);



    for (size_t j = 0; j < image.width; j++) {
        for (size_t i = 0; i < image.height; i++) {
            image.data[pixels_index_of_img(i, j, image)] = (rotate_img->data)[pixels_index_of_rot_img(i, j, image)];
        }
    }

    return image;
}
