#include "bmp.h"
#include "close_f.h"
#include "image.h"
#include "open_f.h"


#include <stdlib.h>


struct image img_from_file(char *file_path) {
    FILE *input = NULL;

    struct image NULL_IMG = (struct image) {
            .width = 0,
            .height = 0,
            .data = NULL
    };


    if (open(file_path, &input, 0) != OPEN) {
        return NULL_IMG;
    }

    struct image image;

    enum read_status read_s = from_bmp(input, &image);
    if (read_s != READ_OK) { close(&input); }


    if (read_s != READ_OK) {
        return NULL_IMG;
    }

    // close the input file
    if (close(&input) == CLOSE_ERROR) {
        return NULL_IMG;
    }

    return image;
}

bool img_to_file(char *file_path, const struct image img) {
    FILE *output = NULL;


    // open the output file
    switch (open(file_path, &output, 1)) {
        case OPEN:
            break;

        case OPEN_ERROR:
            fprintf(stderr, "the output file is not open");
            return false;
    }

    //writing a rotated image to file
    enum status_ status_ = bmp_file(output, &img);
    if (status_ != WRITE_OK) {
        close(&output);
        free(img.data);
        return false;
    }


    // close the output file
    if (close(&output) == CLOSE_ERROR) {
        return false;
    }
    return true;

}

struct image new_img(uint64_t height, uint64_t width) {
    struct image image = {
            .height=height,
            .width=width,
            .data=(struct pixel *) malloc(sizeof(struct pixel) * height * width)

    };
    return image;
}


void free_img(struct image img) {
    img.height = 0;
    img.width = 0;
    free(img.data);
}




