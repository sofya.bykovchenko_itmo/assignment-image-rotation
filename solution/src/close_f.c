#include "close_f.h"

enum close_file enumcloseF(FILE **file) {
    if (fclose(*file) != EOF) {
        return CLOSE;
    } else {
        return CLOSE_ERROR;
    }
}
